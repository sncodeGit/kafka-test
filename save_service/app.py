from flask import Flask, request
from flask_restful import Api, Resource, reqparse
from kafka import KafkaConsumer
from json import dumps, loads
#
import config as cfg


consumer = KafkaConsumer( 
    'nums', 
    bootstrap_servers = cfg.KAFKA_SERVERS, 
    auto_offset_reset = 'earliest', 
    enable_auto_commit = True, 
    group_id = 'save_service', 
    value_deserializer = lambda x : loads(x.decode('utf-8')) 
) 

consumer.subscribe(['save_ugs'])

while True:
    for message in consumer:
        num = message.value['num']
        print(f'get: {num}')

#unsubscribe and close consumer
consumer.unsubscribe()
consumer.close()