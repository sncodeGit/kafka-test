# kafka-test

Задание из университета

## Сервисы

* api_service - 2 реплики, принимает запросы по адресу `/api/v1/add`. Для добавления числа 5000 необходимо отправить GET-запрос на `/api/v1/add/5000`. Полученные числа отправляет в топик `save_ugs`.
* save_service - 2 реплики, принимает запросы из топика `save_ugs` и выводит в лог docker-контейнера.
* kafka - 3 брокера kafka.
* zookeeper
* kafdrop

## Запуск

```
docker-compose up -d --build
# После запуска смотрим, на каких портах слушают api_service
curl -X GET localhost:<PORT1>/api/v1/add/1
curl -X GET localhost:<PORT2>/api/v1/add/2
# Смотрим логи docker-контейнеров save_service
docker logs <SAVE_SERVICE_1_ID> 2>&1 | grep get
docker logs <SAVE_SERVICE_2_ID> 2>&1 | grep get
# Посмотреть все, что относится к kafka можно на http://localhost:9000
```

## Создать топик вручную

```
# Остановим сервисы
docker-compose stop api_service
docker-compose stop save_service
# В интерфейсе kafdrop создаем топик с нужным кол-вом партиций и нужной replica_factor
# Заново запустим сервисы
docker-compose start api_service
docker-compose start save_service
```