import random
import numpy
import requests
#
from datetime import datetime
from flask import Flask, request
from flask_restful import Api, Resource, reqparse
from kafka import KafkaProducer
from json import dumps 
#
import config as cfg


def main(num):
    data = {'num' : num } 
    producer.send('save_ugs', value = data)
    return "Да!"

#######################

app = Flask(__name__)
api = Api(app)

# initializing the Kafka producer 
producer = KafkaProducer( 
    bootstrap_servers = cfg.KAFKA_SERVERS, 
    value_serializer = lambda x:dumps(x).encode('utf-8') 
)

#######################

class GetInt(Resource):
    def get(self, num=0):
        ans = main(num)
        return ans, 200
    
api.add_resource(GetInt, "/api/v1/add", "/api/v1/add/", "/api/v1/add/<int:num>")

if __name__ == '__main__':
    app.run(debug=True)
    producer.close()